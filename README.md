# kafka-streams-geolocation


This application gets an ID and an IP and gets its geolocation. This data is cached for 30 minutes. All new requests within this period, from the same ID and IP, will not be directed to the IPStack API, they will only be obtained from the cache.


#### Technologies

[Java 1.8](https://docs.oracle.com/javase/8/docs/)<br/>
[Maven](https://maven.apache.org/)<br/>
[Kafka Streams](https://kafka.apache.org/documentation/streams/)<br/>
[Redis](https://redis.io/docs/)<br/>
[Docker](https://docs.docker.com/)<br/>
[Docker compose](https://docs.docker.com/compose/)<br/>
[JUnit](https://junit.org/junit5/docs/current/user-guide/)<br/>
[Lombok](https://projectlombok.org/features/all)<br/>
[IPStack](https://ipstack.com/)<br/>


#### Requisites

- Linux operation system
- JRE 1.8
- Docker
- Docker compose
- Maven (to run tests)


#### Execution

1. Open a terminal.
1. Clone this project: `git clone https://gitlab.com/ersdiniz/kafka-streams-geolocation.git`
1. Access the projet folder: `cd kafka-streams-geolocation`
1. Up the Redis, the Zookeeper and the Kafka on Docker: `docker-compose up -d`
1. Run the application in the already generated JAR: `sh run.sh`
1. In another terminal, run the consumer (listen the output topic): `sh run_consumer.sh`
1. In another terminal, run the producer, passing the id and the ip of client: `sh run_producer.sh <id> <ip>`
    - Run as many times as you want...


#### Tests

Run `mvn tests`
