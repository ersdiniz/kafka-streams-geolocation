#!/bin/bash

docker exec -it kafka sh /bin/kafka-topics --create \
    --bootstrap-server localhost:9092 \
    --replication-factor 1 \
    --partitions 1 \
    --topic streams-ip-input

docker exec -it kafka sh /bin/kafka-topics --create \
    --bootstrap-server localhost:9092 \
    --replication-factor 1 \
    --partitions 1 \
    --topic streams-geolocation-output \
    --config cleanup.policy=compact

java -jar lib/geolocation.jar