package br.com.geolocation.protocol;

import static org.junit.Assert.*;

import org.junit.Test;

import br.com.geolocation.ipstack.IpStackDto;

public class GeolocationResponseTest {

	@Test
	public void shouldReturnResponseWithIdAndIpAndTimeOnly() {
		// given
		final String id = "123";
		final String ip = "127.0.0.1";
		final long time = 12345l;
		
		// when
		final GeolocationResponse response = GeolocationResponse.of(id, ip, time);
		
		// then
		assertEquals(id, response.getId());
		assertEquals(ip, response.getIp());
		assertEquals(time, response.getTime());
		assertNull(response.getLatitude());
		assertNull(response.getLongitude());
		assertNull(response.getCountry());
		assertNull(response.getRegion());
		assertNull(response.getCity());
	}

	@Test
	public void shouldReturnResponseComplete() {
		// given
		final String id = "123";
		final String ip = "127.0.0.1";
		final long time = 12345l;
		final String latitude = "-27.596019744423347";
		final String longitude = "-48.55625912327344";
		final String countryName = "Brasil";
		final String regionCode = "SP";
		final String city = "Campinas";
		
		final GeolocationResponse parcialResponse = GeolocationResponse.of(id, ip, time);
		
		final IpStackDto dto = mockIpStackDto(latitude, longitude, countryName, regionCode, city);
		
		// when
		final GeolocationResponse response = GeolocationResponse.of(parcialResponse, dto);
		
		// then
		assertEquals(id, response.getId());
		assertEquals(ip, response.getIp());
		assertEquals(time, response.getTime());
		assertEquals(latitude, response.getLatitude());
		assertEquals(longitude, response.getLongitude());
		assertEquals(countryName, response.getCountry());
		assertEquals(regionCode, response.getRegion());
		assertEquals(city, response.getCity());
	}
	
	private IpStackDto mockIpStackDto(final String latitude, final String longitude, final String countryName, final String regionCode, final String city) {
		final IpStackDto dto = new IpStackDto();
		dto.setLatitude(latitude);
		dto.setLongitude(longitude);
		dto.setCountry_name(countryName);
		dto.setRegion_code(regionCode);
		dto.setCity(city);
		return dto;
	}
}
