package br.com.geolocation.kafka;

import static br.com.geolocation.kafka.KafkaExecutor.getTopology;
import static br.com.geolocation.kafka.KafkaProperties.getProperties;
import static br.com.geolocation.kafka.KafkaTopics.TOPIC_STREAMS_GEOLOCATION_OUTPUT;
import static br.com.geolocation.kafka.KafkaTopics.TOPIC_STREAMS_IP_INPUT;
import static java.lang.String.valueOf;
import static java.lang.System.currentTimeMillis;
import static org.apache.kafka.common.serialization.Serdes.String;
import static org.junit.Assert.assertEquals;

import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.Test;

public class KafkaExecutorIntegrationTest {

	@Test
	public void shouldReturnGeolocation() {
		//given
		final String expectedResult = "{\"id\"=135, \"ip\":\"191.191.0.230\", \"time\":1656612788, \"latitude\":\"-27.596019744873047\", \"longitude\":\"-48.55625915527344\", \"country\":\"Brazil\", \"region\":\"SC\", \"city\":\"Campinas\"}";
		
		final TopologyTestDriver testDriver = new TopologyTestDriver(getTopology(), getProperties());
		
		// when
		final TestInputTopic<String, String> inputTopic = testDriver
				.createInputTopic(TOPIC_STREAMS_IP_INPUT, String().serializer(), String().serializer());
		
		inputTopic.pipeInput(valueOf(currentTimeMillis()), "{\"id\":135,\"ip\":\"191.191.0.230\",\"time\":\"1656612788\"}");
		
		final TestOutputTopic<String, String> outputTopic = testDriver
				.createOutputTopic(TOPIC_STREAMS_GEOLOCATION_OUTPUT, String().deserializer(), String().deserializer());

		// then
		assertEquals(expectedResult, outputTopic.readValue());
	}
}
