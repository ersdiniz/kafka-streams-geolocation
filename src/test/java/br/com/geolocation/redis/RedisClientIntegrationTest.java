package br.com.geolocation.redis;

import static org.junit.Assert.*;

import org.junit.Test;

public class RedisClientIntegrationTest {

	@Test
	public void shouldReturnValueFromRedis() {
		// given
		final String key = "key1";
		final String value = "value1";
		
		// when
		RedisClient.set(key, value);
		
		// then
		assertEquals(value, RedisClient.get(key));
	}

	@Test
	public void shouldNotReturnValueFromRedisWithInvalidKey() {
		// given
		final String key = "key1";
		final String wrongKey = "wrongKey";
		final String value = "value1";
		
		// when
		RedisClient.set(key, value);
		
		// then
		assertNull(RedisClient.get(wrongKey));
	}
}
