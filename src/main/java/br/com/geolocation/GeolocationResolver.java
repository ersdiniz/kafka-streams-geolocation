package br.com.geolocation;

import java.io.IOException;

import br.com.geolocation.ipstack.IpStackConnector;
import br.com.geolocation.ipstack.IpStackDto;
import br.com.geolocation.protocol.GeolocationRequest;
import br.com.geolocation.protocol.GeolocationResponse;
import br.com.geolocation.redis.RedisClient;

public class GeolocationResolver {

	public static String resolve(final GeolocationRequest request) throws IOException {
		final GeolocationResponse response = GeolocationResponse.of(request.getId(), request.getIp(), request.getTime());
		
		final String cacheKey = getKey(request);
		
		final String responseFromCache = RedisClient.get(cacheKey);
		if (responseFromCache != null) {
			return responseFromCache;
		}

		return getResponseFromRemote(response, cacheKey);
	}

	private static String getResponseFromRemote(final GeolocationResponse response, final String cacheKey) throws IOException {
		final IpStackDto dto = IpStackConnector.doGet(response.getIp());
		if (dto == null) {
			return "Failed to access api.ipstack.com";
		}
		
		final String responseFromRemote = GeolocationResponse.of(response, dto).toString();
		
		RedisClient.set(cacheKey, responseFromRemote);
		
		return responseFromRemote;
	}
	
	private static String getKey(final GeolocationRequest request) {
		return request.getId() + ":" + request.getIp();
	}
}
