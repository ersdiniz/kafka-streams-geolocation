package br.com.geolocation.mapper;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JacksonObjectMapper {

	private static final ObjectMapper MAPPER = new ObjectMapper();
    
    static {
        MAPPER.configure(SerializationFeature.WRITE_DATE_KEYS_AS_TIMESTAMPS, true);
        MAPPER.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
    }
    
    @SuppressWarnings("unchecked")
	public static <T> T toBean(String json, Class<?> beanClass) throws JsonProcessingException {
        return (T) MAPPER.readValue(json, beanClass);
    }
}
