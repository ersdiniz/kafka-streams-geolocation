package br.com.geolocation.redis;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.SetParams;

public class RedisClient {

	private static final Jedis JEDIS;
	private static final SetParams PARAMS;
	private static final long EXPIRE = 1800l;
	
	static {
		JEDIS = new Jedis(new HostAndPort("localhost", 6379));
		PARAMS = SetParams.setParams();
		PARAMS.ex(EXPIRE);
    }
	
	public static void set(final String key, final String value) {
		JEDIS.set(key, value, PARAMS);
	}
	
	public static String get(final String key) {
		return JEDIS.get(key);
	}
}
