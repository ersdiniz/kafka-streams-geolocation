package br.com.geolocation;

import br.com.geolocation.kafka.KafkaExecutor;

public class GeolocationMain {

	public static void main(final String[] args) throws Exception {
		KafkaExecutor.execute();
    }
}
