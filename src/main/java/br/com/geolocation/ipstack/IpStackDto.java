package br.com.geolocation.ipstack;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpStackDto implements Serializable {

	private String latitude;
	private String longitude;
	private String country_name;
	private String region_code;
	private String city;
}
