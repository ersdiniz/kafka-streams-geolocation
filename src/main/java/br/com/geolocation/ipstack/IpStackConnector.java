package br.com.geolocation.ipstack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import br.com.geolocation.mapper.JacksonObjectMapper;

public class IpStackConnector {
	
	// This constant need to be a external parameter
	private static final String ACCESS_KEY = "70145bad15d03db564fecd36ac874a4d";
	private static final String HOST = "http://api.ipstack.com/%s?access_key=%s&format=1";
	private static final String METHOD_GET = "GET";

	public static IpStackDto doGet(final String ip) throws IOException {
		final URL url = new URL(String.format(HOST, ip, ACCESS_KEY));
		final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod(METHOD_GET);
		
		final InputStream inputStream = getInputStream(connection);
		if (inputStream == null) {
			return null;
		}

	    return JacksonObjectMapper.toBean(getResponse(inputStream), IpStackDto.class);
	}

	private static InputStream getInputStream(final HttpURLConnection connection) throws IOException {
		final int responseCode = connection.getResponseCode();
	    if (200 >= responseCode && responseCode < 300) {
	    	return connection.getInputStream();
	    }
	    return null;
	}

	private static String getResponse(final InputStream inputStream) throws IOException {
		final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

	    final StringBuilder response = new StringBuilder();
	    String currentLine;

	    while ((currentLine = bufferedReader.readLine()) != null) {
	        response.append(currentLine);
	    }

	    bufferedReader.close();
		return response.toString();
	}
}
