package br.com.geolocation.kafka;

import static br.com.geolocation.GeolocationResolver.resolve;
import static br.com.geolocation.kafka.KafkaProperties.getProperties;
import static br.com.geolocation.kafka.KafkaTopics.TOPIC_STREAMS_GEOLOCATION_OUTPUT;
import static br.com.geolocation.kafka.KafkaTopics.TOPIC_STREAMS_IP_INPUT;
import static br.com.geolocation.mapper.JacksonObjectMapper.toBean;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;

import br.com.geolocation.protocol.GeolocationRequest;

public class KafkaExecutor {

	public static void execute() {
		final Topology topology = getTopology();

        run(new KafkaStreams(topology, getProperties()));
        
        System.exit(0);
	}
	
	protected static Topology getTopology() {
		final StreamsBuilder builder = new StreamsBuilder();
        
        final KStream<String, String> source = builder.stream(TOPIC_STREAMS_IP_INPUT);
        
        source.mapValues(KafkaExecutor::getGeolocation).to(TOPIC_STREAMS_GEOLOCATION_OUTPUT);
        
        return builder.build();
	}

	private static String getGeolocation(final String value) {
		try {
			return resolve(toBean(value, GeolocationRequest.class));
		} catch (IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}

	private static void run(final KafkaStreams streams) {
		final CountDownLatch latch = new CountDownLatch(1);
		
		Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
	}
}
