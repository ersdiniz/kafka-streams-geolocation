package br.com.geolocation.kafka;

public class KafkaTopics {

	public static final String TOPIC_STREAMS_IP_INPUT = "streams-ip-input";
	
	public static final String TOPIC_STREAMS_GEOLOCATION_OUTPUT = "streams-geolocation-output";
}
