package br.com.geolocation.kafka;

import static org.apache.kafka.streams.StreamsConfig.*;

import java.util.Properties;

import org.apache.kafka.common.serialization.Serdes;

public class KafkaProperties {

	private static final Properties PROPERTIES;
	
	static {
		PROPERTIES = new Properties();
        PROPERTIES.put(APPLICATION_ID_CONFIG, "streams-geolocation");
        PROPERTIES.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        PROPERTIES.put(DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        PROPERTIES.put(DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
	}
	
	public static Properties getProperties() {
		return PROPERTIES;
	}
}
