package br.com.geolocation.protocol;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
public class GeolocationRequest implements Serializable {

	private String id;
	private String ip;
	private long time;
}
