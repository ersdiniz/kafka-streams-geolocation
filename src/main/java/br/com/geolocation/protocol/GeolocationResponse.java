package br.com.geolocation.protocol;

import java.io.Serializable;

import br.com.geolocation.ipstack.IpStackDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("serial")
@Getter
@Setter
@NoArgsConstructor
public class GeolocationResponse implements Serializable {

	private String id;
	private String ip;
	private long time;
	private String latitude;
	private String longitude;
	private String country;
	private String region;
	private String city;

	public static GeolocationResponse of(final String id, final String ip, final long time) {
		final GeolocationResponse response = new GeolocationResponse();
		response.setId(id);
		response.setIp(ip);
		response.setTime(time);
		return response;
	}

	public static GeolocationResponse of(final GeolocationResponse response, final IpStackDto dto) {
		response.setLatitude(dto.getLatitude());
		response.setLongitude(dto.getLongitude());
		response.setCountry(dto.getCountry_name());
		response.setRegion(dto.getRegion_code());
		response.setCity(dto.getCity());
		return response;
	}

	@Override
	public String toString() {
		return "{\"id\"=\"" + id + "\", \"ip\":\"" + ip + "\", \"time\":" + time + ", \"latitude\":\"" + latitude
				+ "\", \"longitude\":\"" + longitude + "\", \"country\":\"" + country + "\", \"region\":\"" + region
				+ "\", \"city\":\"" + city + "\"}";
	}
}
