#!/bin/bash

if [ "$1" = "" ]
then
  echo "The ID of client is required! Sample: sh run_producer.sh <id> <ip>"
  exit
fi

if [ "$2" = "" ]
then
  echo "The IP of client is required! Sample: sh run_producer.sh <id> <ip>"
  exit
fi

NOW=$(date +%s)

echo $NOW': {"id":"'$1'","ip":"'$2'","time":"'$NOW'"}' > data.json

docker exec -i kafka bash /bin/kafka-console-producer --bootstrap-server localhost:9092 --property "parse.key=true" --property "key.separator=:" --topic streams-ip-input < data.json