#!/bin/bash

docker exec -it kafka sh /bin/kafka-console-consumer --bootstrap-server localhost:9092 --topic streams-geolocation-output --from-beginning